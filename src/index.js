import {
    error,
    success,
} from 'consola';
import {
    DB,
    PORT,
    IN_PROD
} from './config';
import {
    ApolloServer,ApolloError
} from 'apollo-server-express';
import express from 'express';
import mongoose from 'mongoose';
import typeDefs from './graphql/typeDefs';
import resolvers from './graphql/resolvers';
import * as AppModels from './models';
import AuthMiddleware from './middlewares/auth';
import bodyParser from 'body-parser';
const { makeExecutableSchema } = require('@graphql-tools/schema');
const { mapSchema, getDirective, MapperKind } = require('@graphql-tools/utils');
const { defaultFieldResolver } = require('graphql');

const app = express();

// Remove x-powered-by header
app.disable("x-powered-by");
app.use(AuthMiddleware);
app.use(bodyParser.json());


function checkDirectiveTransformer(schema, directiveName) {
    return mapSchema(schema, {
  
      // Executes once for each object field in the schema
      [MapperKind.OBJECT_FIELD]: (fieldConfig) => {
  
        const { resolve = defaultFieldResolver } = fieldConfig;

        const upperDirective = getDirective(schema, fieldConfig, directiveName);
       
        if (typeof(upperDirective) != "undefined") {
        
            fieldConfig.resolve = async function (...args) {
                let [_, {}, {
                    user,
                    isAuth
                }] = args;
                
                if (isAuth) {
                    const result = await  resolve.apply(this, args);
                    return result;
                } else {
                    throw new ApolloError(
                        'You must be the authenticated user to get this information'
                    );
                }
            }
            
            return fieldConfig;
        }
      }
    });
  }

  // Create the base executable schema
let schema = makeExecutableSchema({
    typeDefs,
    resolvers
  });
  
// Transform the schema by applying directive logic
schema = checkDirectiveTransformer(schema, 'isAuth');

// Define the Apollo-Server
const server = new ApolloServer({
    schema,
    playground: !IN_PROD,
    context: ({
        req
    }) => {

        let {
            user,
            isAuth,
        } = req;

        return {
            req,
            user,
            isAuth,
            ...AppModels,
        };
    }
});

// Function to start express and apollo server 
const startApp = async () => {
    try {

        // Connect With MongoDB Database
        await mongoose.connect(DB, {
            auth: { "authSource": "admin" },
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        });
        success({
            badge: true,
            message: `Successfully connected with the database ${DB}`,
        });

        await server.start();

        // Apply Apollo-Express-Server Middlware to express application
        server.applyMiddleware({
            app,
            cors:true
        });

        // Start Listening on the Server
        app.listen(PORT, () =>
            success({
                badge: true,
                message: `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`,
            })
        );
    } catch (err) {
        error({
            badge: true,
            message: err.message
        });
    }
}

// Invoke Start Application Function
startApp();