import {
    hash,
    compare,
} from 'bcryptjs';

import {
    ApolloError
} from 'apollo-server-express';

import {
    serializeUser,
    issueAuthToken
} from '../../helpers/Userfunctions';

export default {
    Query: {
        /**
         * @DESC to authenticate using parameters
         * @Params { username, password }
         * @Access Public
         */
        login: async (_, {
            username,
            password
        }, {
            User
        }) => {
            
            // Find the user from the database
            let user = await User.findOne({
                username
            });
            // If User is not found
            if (!user) {
                throw new ApolloError("Username not found", '404');
            }
            // If user is found then compare the password
            let isMatch = await  compare(password, user.password);
            // If Password don't match
            if (!isMatch) {
                throw new ApolloError("Wrong password", '403');
            }
            user = await serializeUser(user);
            // Issue Token
            let token = await issueAuthToken(user);
            return {
                user,
                token,
            }
        }
    },
    Mutation: {
        /**
         * @DESC to Create new user
         * @Params newUser{ username, password }
         * @Access Public
         */
        createUser: async (_, {
            newUser
        }, {
            User
        }) => {
            try {
            
                let {
                    username,
                    password
                } = newUser;

                // Check if the Username is taken
                let user = await User.findOne({
                    username
                });
                if (user) {
                    throw new ApolloError('Username is already taken.', '403')
                }

                // New User's Account can be created
                user = new User(newUser);

                // Hash the user password
                console.log(password);
                user.password = await hash(password, 10);

                // Save the user to the database
                let result = await user.save();
                result = await serializeUser(result);
                
                // Issue Token
                let token = await issueAuthToken(result);
                return {
                    token,
                    user: result
                }
            } catch (err) {
                throw new ApolloError(err.message);
            }
        }
    }
}