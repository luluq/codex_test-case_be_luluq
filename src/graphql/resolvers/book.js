import {
    ApolloError
} from 'apollo-server-express';

export default {
    Query: {
        /**
         * @DESC to Get all Book
         * @Access Guest
         */
        getBooks: async (_, {}, {
            Book
            }) => {
                let books = await Book.find();
                return books;
        },
        /**
         * @DESC to Get single Book by ID
         * @Access Guest
         */
         getBook: async (_, {
            id
        }, {
            Book
        }) => {
            try {
                let book = await Book.findById(id);
                return book;
            } catch (err) {
                throw new ApolloError(err.message);
            }
        },
    },
    Mutation: {
        /**
         * @DESC to Create new Book
         * @Params newBook{ 
                title!, 
                description!
            }
         * @Access Admin Access
         */
        createBook: async (_, {
            newBook
        }, {
            Book
        }) => {
            try {
                const {
                    title,
                    description,
                } = newBook;
    
                const book = new Book({
                    ...newBook
                });
                // Save the book
                let result = await book.save();
                result = {
                    ...result.toObject(),
                    id: result._id.toString()
                }

                return result;
            } catch (err) {
                throw new ApolloError(err.message);
            }
        },
        
        /**
         * @DESC to Update an Existing Book by ID
         * @Params updatedBook { 
                title!, 
                description!,
            }
         * @Access Admin Access
         */
        updateBook: async (_, {
            updatedBook,
            id,
        }, {
            Book
        }) => {
            try {
                let {
                    title,
                    description
                } = updatedBook;

                let book = await Book
                    .findOneAndUpdate({
                            _id: id,
                        },
                        updatedBook, {
                            new: true
                        }
                    );

                if (!book) {
                    throw new Error("Unathorized Access");
                }

                return book;
            } catch (err) {
                throw new ApolloError(err.message);
            }
        },

        /**
         * @DESC to Delete an Existing Book by ID
         * @Params id!
         * @Access Private
         */
         deleteBook: async (_, {
            id,
        }, {
            Book
        }) => {
            try {
                let book = await Book.findOneAndDelete({
                    _id: id,
                });

                if (!book) {
                    throw new Error("Unathorized Access");
                }
                return {
                    success: true,
                    message: "Book Deleted Successfully."
                }
            } catch (err) {
                throw new ApolloError(err.message);
            }
        }
    }
}