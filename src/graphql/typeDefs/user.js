import {
    gql
} from "apollo-server-express";

export default gql `
    extend type Query {
        authUser: User!
        login(username: String!, password: String!):AuthUser!
    }

    extend type Mutation {
        createUser(newUser: UserInput!): AuthUser!
    }

    input UserInput {
        username:String!
        password: String!
    }

    type User {
        id: ID!
        username:String!
        password:String
        createdAt: String
        updatedAt: String
    }

    type AuthUser {
        user: User!
        token:String!
    }
`;