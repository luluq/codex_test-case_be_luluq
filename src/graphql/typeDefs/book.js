import {
    gql
} from 'apollo-server-express';

export default gql `
    extend type Query {
       getBooks: [Book!]!,
       getBook(id: ID!): Book!
    }

    extend type Mutation{
        createBook(newBook: BookInput): Book! @isAuth
        updateBook(updatedBook: BookInput, id: ID!): Book! @isAuth
        deleteBook(id: ID!): BookMessageResponse! @isAuth
    }

    input BookInput {
        title: String!
        description: String!
    }

    type Book {
        id: ID!,
        title: String!
        description: String!
        createdAt: String!
        updatedAt: String!
    }

    type BookMessageResponse {
        message: String!
        success: Boolean
    }
`