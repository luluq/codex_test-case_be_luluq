import {
    model,
    Schema
} from 'mongoose';

const BookSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

const Book = model('books', BookSchema);
export default Book;