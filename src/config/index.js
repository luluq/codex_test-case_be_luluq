import {
    config
} from 'dotenv';

const {
    parsed
} = config();

export const {
    DB,
    PORT=8000,
    PROD,
    SECRET,
    IN_PROD = PROD === 'prod',
    BASE_URL = `http://localhost:${PORT}`,
} = parsed;